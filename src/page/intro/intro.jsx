import Nerv from 'nervjs';

class Intro extends Nerv.Component {
  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        <img src="https://thumbs.gfycat.com/FaroffVagueAustralianshelduck-size_restricted.gif" />
      </div>
    );
  }
}

export default Intro;
