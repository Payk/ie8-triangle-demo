import Nerv from 'nervjs';

import './index.css';

import TabBar from '../component/tabBar/tabBar';

import Intro from './intro/intro';
import CssTriangleDemo from './cssTriangleDemo/cssTriangleDemo';
import ArbitraryTriangleDemo from './arbitraryTriangleDemo/arbitraryTriangleDemo';

class App extends Nerv.Component {
  render() {
    return (
      <TabBar
        tabs={[
          {
            title: 'Intro',
            cmp: <Intro />
          },
          {
            title: 'Right triangle with css',
            cmp: <CssTriangleDemo />
          },
          {
            title: 'Cut Arbitrary triangle into right triangles',
            cmp: <ArbitraryTriangleDemo />
          }
        ]}
      />
    );
  }
}

Nerv.render(<App />, document.querySelector('#app'));
