function createRightTriangle(p1, p2, p3, above) {
  let triangleType = '';

  if (p1.y < p2.y) {
    if (above) {
      // var result = "<div style=\"position:absolute;height:0px;left:" + Math.floor(p1.x) + "px;top:" + Math.ceil(p1.y) + "px;border-left-style:solid;border-top-style:solid;";
      // result += "border-left-color:" + self.transparentColor + ";border-top-color:" + color;
      // result += ";border-left-width:" + Math.ceil(p2.x - p1.x + 1) + "px;border-top-width:" + Math.ceil(p2.y - p1.y + 1) + "px;\"></div>";
      // trianglesHtml.push(result);

      triangleType = 'topleft';
    } else {
      // var result = "<div style=\"position:absolute;height:0px;left:" + Math.ceil(p1.x) + "px;top:" + Math.floor(p1.y) + "px;border-right-style:solid;border-bottom-style:solid;";
      // result += "border-right-color:" + self.transparentColor + ";border-bottom-color:" + color;
      // result += ";border-right-width:" + Math.ceil(p2.x - p1.x + 1) + "px;border-bottom-width:" + Math.ceil(p2.y - p1.y + 1) + "px;\"></div>";
      // trianglesHtml.push(result);

      triangleType = 'topright';
    }
  } else {
    if (above) {
      // var result = "<div style=\"position:absolute;height:0px;left:" + Math.ceil(p1.x) + "px;top:" + Math.ceil(p2.y) + "px;border-right-style:solid;border-top-style:solid;";
      // result += "border-right-color:" + self.transparentColor + ";border-top-color:" + color;
      // result += ";border-right-width:" + Math.ceil(p2.x - p1.x + 1) + "px;border-top-width:" + Math.ceil(p1.y - p2.y + 1) + "px;\"></div>";
      // trianglesHtml.push(result);
      triangleType = 'bottomright';
    } else {
      // var result = "<div style=\"position:absolute;height:0px;left:" + Math.floor(p1.x) + "px;top:" + Math.floor(p2.y) + "px;border-left-style:solid;border-bottom-style:solid;";
      // result += "border-left-color:" + self.transparentColor + ";border-bottom-color:" + color;
      // result += ";border-left-width:" + Math.ceil(p2.x - p1.x + 1) + "px;border-bottom-width:" + Math.ceil(p1.y - p2.y + 1) + "px;\"></div>";
      // trianglesHtml.push(result);
      triangleType = 'bottomleft';
    }
  }

  return {
    left: p1.x,
    top: p1.y,
    type: triangleType,
    width1: p2.y - p1.y,
    width2: p2.x - p1.x
  };
}

function splitFlatBottomTriangle(op1, op2, op3) {
  const rightTriangles = [];
  const p1 = op1;
  let p2 = op2;
  let p3 = op3;

  if (p1.x <= p2.x) {
    const dy = (p3.y - p1.y) / (p3.x - p1.x);
    const dx = (p2.x - p1.x) / (p2.y - p1.y);

    while (p2.x - p1.x > 1) {
      const p4 = { x: p2.x, y: p1.y + (p2.x - p1.x) * dy };
      const p5 = { y: p4.y, x: p1.x + (p4.y - p1.y) * dx };

      rightTriangles.push(createRightTriangle(p4, p3, false));
      rightTriangles.push(createRightTriangle(p5, p2, true));

      p2 = p5;
      p3 = p4;
    }

    rightTriangles.push(createRightTriangle(p1, p3, false));
  } else if (p1.x >= p3.x) {
    const dy = (p2.y - p1.y) / (p2.x - p1.x);
    const dx = (p3.x - p1.x) / (p3.y - p1.y);

    while (p1.x - p3.x > 1) {
      const p4 = { x: p3.x, y: p1.y + (p3.x - p1.x) * dy };
      const p5 = { y: p4.y, x: p1.x + (p4.y - p1.y) * dx };

      rightTriangles.push(createRightTriangle(p2, p4, false));
      rightTriangles.push(createRightTriangle(p3, p5, true));

      p2 = p4;
      p3 = p5;
    }

    rightTriangles.push(createRightTriangle(p2, p1, false));
  } else {
    rightTriangles.push(createRightTriangle(p2, p1, false));
    rightTriangles.push(createRightTriangle(p1, p3, false));
  }

  return rightTriangles;
}

function splitFlatTopTriangle(op1, op2, op3) {
  const rightTriangles = [];
  const p1 = op1;
  let p2 = op2;
  let p3 = op3;

  if (p1.x <= p2.x) {
    const dy = (p3.y - p1.y) / (p3.x - p1.x);
    const dx = (p2.x - p1.x) / (p2.y - p1.y);

    while (p2.x - p1.x > 1) {
      const p4 = { x: p2.x, y: p1.y + (p2.x - p1.x) * dy };
      const p5 = { y: p4.y, x: p1.x + (p4.y - p1.y) * dx };

      rightTriangles.push(createRightTriangle(p4, p3, true));
      rightTriangles.push(createRightTriangle(p5, p2, false));

      p2 = p5;
      p3 = p4;
    }

    rightTriangles.push(createRightTriangle(p1, p3, true));
  } else if (p1.x >= p3.x) {
    const dy = (p2.y - p1.y) / (p2.x - p1.x);
    const dx = (p3.x - p1.x) / (p3.y - p1.y);

    while (p1.x - p3.x > 1) {
      const p4 = { x: p3.x, y: p1.y + (p3.x - p1.x) * dy };
      const p5 = { y: p4.y, x: p1.x + (p4.y - p1.y) * dx };

      rightTriangles.push(createRightTriangle(p2, p4, true));
      rightTriangles.push(createRightTriangle(p3, p5, false));

      p2 = p4;
      p3 = p5;
    }

    rightTriangles.push(createRightTriangle(p2, p1, true));
  } else {
    rightTriangles.push(createRightTriangle(p2, p1, true));
    rightTriangles.push(createRightTriangle(p1, p3, true));
  }

  return rightTriangles;
}

function compareY(p1, p2) {
  return p1.y - p2.y;
}

function splitTriangle(points) {
  let rightTriangles = [];
  points.sort(compareY);
  const p1 = points[0];
  const p2 = points[1];
  const p3 = points[2];

  if (p2.y - p1.y < 0) {
    if (p1.x > p2.x) {
      rightTriangles = rightTriangles.concat(splitFlatTopTriangle(p3, p2, p1));
    } else {
      rightTriangles = rightTriangles.concat(splitFlatTopTriangle(p3, p1, p2));
    }
  } else if (p3.y - p2.y < 0) {
    if (p2.x > p3.x) {
      rightTriangles = rightTriangles.concat(
        splitFlatBottomTriangle(p1, p3, p2)
      );
    } else {
      rightTriangles = rightTriangles.concat(
        splitFlatBottomTriangle(p1, p2, p3)
      );
    }
  } else {
    const p4 = {
      y: p2.y,
      x: p1.x + (p2.y - p1.y) * ((p3.x - p1.x) / (p3.y - p1.y))
    };

    if (p2.x > p4.x) {
      rightTriangles = rightTriangles.concat(
        splitFlatBottomTriangle(p1, p4, p2)
      );
      rightTriangles = rightTriangles.concat(splitFlatTopTriangle(p3, p4, p2));
    } else {
      rightTriangles = rightTriangles.concat(
        splitFlatBottomTriangle(p1, p2, p4)
      );
      rightTriangles = rightTriangles.concat(splitFlatTopTriangle(p3, p2, p4));
    }
  }

  return rightTriangles;
}

export default splitTriangle;
