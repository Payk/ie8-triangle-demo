import Nerv from 'nervjs';

import './cssTriangleDemo.css';

class CssTriangleDemo extends Nerv.Component {
  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        <div class="sampleTriangle" />
      </div>
    );
  }
}

export default CssTriangleDemo;
