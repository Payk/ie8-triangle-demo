import Nerv from 'nervjs';

import './tabBar.css';

class TabBar extends Nerv.Component {
  constructor() {
    super(...arguments);
    this.state = {
      index: 0
    };

    this.tabs = this.props.tabs || [];
  }

  render() {
    const tabList = this.tabs.map(function(tabItem, index) {
      return (
        <td
          valign="center"
          className={this.state.index === index ? 'active' : ''}
          onClick={() => {
            this.setState({ index: index });
          }}>
          {tabItem.title}
        </td>
      );
    }, this);

    return (
      <table className="tabBar">
        <tr className="headRow">{tabList}</tr>
        <tr className="contentRow">
          <td colspan={this.tabs.length} valign="center">
            {this.tabs[this.state.index].cmp}
          </td>
        </tr>
      </table>
    );
  }
}

export default TabBar;
