IE8 triangle demo
---

Using nervjs/jsx/typescript to render 3D in old IE8+ browsers
using right css triangles as shown in https://www.uselesspickles.com/triangles/

## Usage

* Install:
    `npm install`

* Run dev server:
    `npm run dev`

* Build:
    `npm run build`

* Run in IE 8
    `npm run deploy`

* Start tunnel (for lambda test)
    `npm run deploy`
