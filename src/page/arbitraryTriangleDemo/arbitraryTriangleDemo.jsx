import Nerv from 'nervjs';

import './arbitraryTriangleDemo.css';

import splitTriangle from '../../utils/triangleSplitting';

function addListener(element, type, callback, capture) {
  if (element.addEventListener) {
    element.addEventListener(type, callback, capture);
  } else {
    element.attachEvent('on' + type, callback);
  }
}

function draggable(element) {
  let dragging = false;

  addListener(
    element,
    'mousedown',
    function(e) {
      e = window.event || e;
      dragging = {
        mouseX: e.clientX,
        mouseY: e.clientY,
        startX: parseInt(element.style.left),
        startY: parseInt(element.style.top)
      };
      if (element.setCapture) element.setCapture();
    },
    false
  );

  addListener(
    element,
    'losecapture',
    function() {
      dragging = null;
    },
    false
  );

  addListener(
    document,
    'mouseup',
    function() {
      dragging = null;
      if (document.releaseCapture) document.releaseCapture();
    },
    true
  );

  const dragTarget = element.setCapture ? element : document;

  addListener(
    dragTarget,
    'mousemove',
    function(e) {
      if (!dragging) return;

      e = window.event || e;
      const top = dragging.startY + (e.clientY - dragging.mouseY);
      const left = dragging.startX + (e.clientX - dragging.mouseX);

      element.style.top = Math.max(0, top) + 'px';
      element.style.left = Math.max(0, left) + 'px';
    },
    true
  );
}

class ArbitraryTriangleDemo extends Nerv.Component {
  constructor(props) {
    super(props);
    this.state = {
      p1: { x: 50, y: 50 },
      p2: { x: 20, y: 100 },
      p3: { x: 80, y: 110 }
    };

    this.setTextInputRef = element => {
      draggable(element);
    };
  }

  render() {
    const triangles = splitTriangle([
      this.state.p1,
      this.state.p2,
      this.state.p3
    ]);
    if (window.console && window.console.log) {
      console.log(triangles);
    }

    return (
      <div
        style={{ textAlign: 'center', position: 'relative', height: '100%' }}>
        <div
          class="repositioningHandle"
          ref={this.setTextInputRef}
          style={{
            left: this.state.p1.x,
            top: this.state.p1.y
          }}>
          1
        </div>

        <div />
      </div>
    );
  }
}

export default ArbitraryTriangleDemo;
